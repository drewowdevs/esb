pragma solidity ^0.4.24;

import "./Ownable.sol";
import "./ERC20Basic.sol";
import "./SafeMath.sol";


/**
 * @title Standard ERC20 token & Crowdsale
 *
 * @dev Implementation of Emerald Space Blockchain token
 */
contract ESB is ERC20Basic, Ownable {

    using SafeMath for uint256;

    string public name = "Emerald Space Token";
    string public symbol = "ESB";
    uint8 public decimals = 18;

    mapping(address => uint256) balances;

    uint256 totalSupply_;

    // number of token per ether
    uint public price;

    // Amount of wei raised
    uint256 public weiRaised;

    event Refund(address indexed refunder, uint256 amount);

    /**
     * Event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param value weis paid for purchase
     * @param amount amount of tokens purchased
     */
    event TokenPurchase(
        address indexed purchaser,
        address indexed beneficiary,
        uint256 value,
        uint256 amount
    );

    mapping (address => mapping (address => uint256)) internal allowed;


    modifier hasMintPermission() {
        require(msg.sender == owner);
        _;
    }

    /**
     * @dev Constructor
     * allocate all tokens to the owner
     */
    constructor(uint _price) public {
        require(_price > 0);

        price = _price;
        totalSupply_ = 10000000 ether;
        balances[this] = totalSupply_;
    }

    /**
    * @dev total number of tokens in existence
    */
    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }

    /**
    * @dev Gets the balance of the specified address.
    * @param _owner The address to query the the balance of.
    * @return An uint256 representing the amount owned by the passed address.
    */
    function balanceOf(address _owner) public view returns (uint256) {
        return balances[_owner];
    }

    /**
    * @dev transfer token for a specified address
    * @param _to The address to transfer to.
    * @param _value The amount to be transferred.
    */
    function transfer(address _to, uint256 _value) public returns (bool) {
        require(_to != address(0));
        require(_value <= balances[msg.sender]);

        _transfer(msg.sender, _to, _value);

        if (_to == address(this)) {
            _refund(_value);
        }

        return true;
    }

    // -----------------------------------------
    // Crowdsale external interface
    // -----------------------------------------

    /**
     * @dev fallback function ***DO NOT OVERRIDE***
     */
    function () external payable {
        buyTokens(msg.sender);
    }

    /**
     * @dev low level token purchase ***DO NOT OVERRIDE***
     * @param _beneficiary Address performing the token purchase
     */
    function buyTokens(address _beneficiary) internal {

        uint256 weiAmount = msg.value;
        _preValidatePurchase(_beneficiary, weiAmount);

        // calculate token amount to be created
        uint256 tokens = _getTokenAmount(weiAmount);

        // update state
        weiRaised = address(this).balance;

        _processPurchase(_beneficiary, tokens);
        emit TokenPurchase(
            msg.sender,
            _beneficiary,
            weiAmount,
            tokens
        );

        _postValidatePurchase(_beneficiary, weiAmount);
    }

    // -----------------------------------------
    // Owner interface (extensible)
    // -----------------------------------------

    /**
     * @dev Set price for ESB token. 1 ETH = X token (x100)
     * @param _price price the users can sell to the contract
     */
    function setPrice(uint256 _price) onlyOwner external {
        require(_price > 0);
        price = _price;
    }

    /**
     * @dev Withdraw tokens to the address
     * @param _beneficiary Address performing the token purchase
     * @param _tokenAmount tokenAmount to withdraw
     */
    function withdraw(address _beneficiary, uint256 _tokenAmount) onlyOwner external {
        _deliverTokens(_beneficiary, _tokenAmount);
    }

    /**
     * @dev Withdraw ETH to the address
     * @param _wallet Address performing the token purchase
     * @param _amount Ether amount to withdraw
     */
    function withdrawEth(address _wallet, uint256 _amount) onlyOwner external {
        _wallet.transfer(_amount);
        weiRaised = address(this).balance;
    }

    // -----------------------------------------
    // Internal interface (extensible)
    // -----------------------------------------
    /**
    * @dev transfer token from specific address
    * @param _from The address to transfer from.
    * @param _to The address to transfer to.
    * @param _value The amount to be transferred.
    */
    function _transfer(
        address _from,
        address _to,
        uint256 _value
    )
    internal
    returns (bool)
    {
        require(_from != address(0));
        require(_to != address(0));
        require(_value <= balances[_from]);

        balances[_from] = balances[_from].sub(_value);
        balances[_to] = balances[_to].add(_value);
        emit Transfer(_from, _to, _value);
        return true;
    }

    /**
   * @dev refund ETH to token holders at current price.
   * @param _tokenAmount token amount to refund
   */
    function _refund(uint256 _tokenAmount) internal {
        uint256 weiAmount = _tokenAmount.div(price).mul(100); // Price is token per ether //* price / ethusd;
        msg.sender.transfer(weiAmount);
        weiRaised = address(this).balance;

        emit Refund(msg.sender, _tokenAmount);
    }

    /**
     * @dev Validation of an incoming purchase. Use require statements to revert state when conditions are not met. Use super to concatenate validations.
     * @param _beneficiary Address performing the token purchase
     * @param _weiAmount Value in wei involved in the purchase
     */
    function _preValidatePurchase(
        address _beneficiary,
        uint256 _weiAmount
    )
    internal
    {
        require(_beneficiary != address(0));
        require(_weiAmount != 0);
    }

    /**
     * @dev Validation of an executed purchase. Observe state and use revert statements to undo rollback when valid conditions are not met.
     * @param _beneficiary Address performing the token purchase
     * @param _weiAmount Value in wei involved in the purchase
     */
    function _postValidatePurchase(
        address _beneficiary,
        uint256 _weiAmount
    )
    internal
    {
        // optional override
    }

    /**
     * @dev Source of tokens. Override this method to modify the way in which the crowdsale ultimately gets and sends its tokens.
     * @param _beneficiary Address performing the token purchase
     * @param _tokenAmount Number of tokens to be emitted
     */
    function _deliverTokens(
        address _beneficiary,
        uint256 _tokenAmount
    )
    internal
    {
        _transfer(this, _beneficiary, _tokenAmount);
    }

    /**
     * @dev Executed when a purchase has been validated and is ready to be executed. Not necessarily emits/sends tokens.
     * @param _beneficiary Address receiving the tokens
     * @param _tokenAmount Number of tokens to be purchased
     */
    function _processPurchase(
        address _beneficiary,
        uint256 _tokenAmount
    )
    internal
    {
        _deliverTokens(_beneficiary, _tokenAmount);
    }

    /**
     * @dev Override to extend the way in which ether is converted to tokens.
     * @param _weiAmount Value in wei to be converted into tokens
     * @return Number of tokens that can be purchased with the specified _weiAmount
     */
    function _getTokenAmount(uint256 _weiAmount)
    internal view returns (uint256)
    {
        return _weiAmount.mul(price).div(100); //token per ether //* ethusd / price;
    }


}
