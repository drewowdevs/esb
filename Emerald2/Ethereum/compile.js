const path = require('path');
const solc = require('solc');
const fs = require('fs-extra');

const buildPath = path.resolve(__dirname, 'build');
fs.removeSync(buildPath);

const esbPath = path.resolve(__dirname, 'contracts', 'ESB.sol');
const erc20Basic = path.resolve(__dirname, 'contracts', 'ERC20Basic.sol');
const ownable = path.resolve(__dirname, 'contracts', 'Ownable.sol');
const safeMath = path.resolve(__dirname, 'contracts', 'SafeMath.sol');

var input = {
    'ESB.sol': fs.readFileSync(esbPath, 'utf8'),
    'ERC20Basic.sol': fs.readFileSync(erc20Basic, 'utf8'),
    'Ownable.sol': fs.readFileSync(ownable, 'utf8'),
    'SafeMath.sol': fs.readFileSync(safeMath, 'utf8')
};

const output = solc.compile({sources: input}, 1).contracts;

console.log(output);
fs.ensureDirSync(buildPath);

for(let contract in output) {
    fs.outputJsonSync (
        path.resolve(buildPath, contract.substring(contract.lastIndexOf(":") + 1) + '.json'),
        output[contract]
    );
}