const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledESB = require('../Ethereum/build/ESB.json');

let accounts;
let esb;

beforeEach( async () => {
    accounts = await web3.eth.getAccounts();

    esb = await new web3.eth.Contract(JSON.parse(compiledESB.interface))
        .deploy({
            data: compiledESB.bytecode,
            arguments: [43000]
        })
        .send({ from: accounts[0], gas: '1000000'});

});

describe('ESB', () => {
    it('2.1.0 Create contract and have owner’s address assigned to it. ', async () => {
        assert.ok(esb.options.address);
        const ownerAddress = await esb.methods.owner().call();
        assert.equal(ownerAddress, accounts[0]);
    });

    it('2.1.1 Set price of contract', async() => {
        try {
            await esb.methods.setPrice('20000').send( {
                from: accounts[0],
                gas: '1000000'
            });

            const currentPrice = await esb.methods.price().call();
            assert.equal(currentPrice, '20000');

            await esb.methods.setPrice('43000').send( {
                from: accounts[1],
                gas: '1000000'
            });
            assert(false);
        }
        catch(err)
        {
            assert(err);
        }
    });

    it('2.1.2 Transfer ownership of contract', async () => {
        await esb.methods.transferOwnership(accounts[1]).send({
            from: accounts[0],
            gas: '1000000'
        });

        const newOwner = await esb.methods.owner().call();
        assert.equal(newOwner, accounts[1]);
    });

    it('2.1.3 Withdraw tokens to specified beneficiary', async () => {
        const tokenAmount = web3.utils.toWei('11', 'ether');
        await esb.methods.withdraw(accounts[2], tokenAmount).send({
            from: accounts[0],
            gas: '1000000'
        });

        const balanceOfBeneficiary = await esb.methods.balanceOf(accounts[2]).call();
        assert.equal(balanceOfBeneficiary, tokenAmount);
    });

    it('2.1.4 Withdraw ether to specific wallet beneficiary', async () => {
        const addressContract = esb.options.address;
        await web3.eth.sendTransaction({
            from: accounts[0],
            to: addressContract,
            value: web3.utils.toWei('10', 'ether')
        });

        const etherAmount = web3.utils.toWei('5', 'ether');
        await esb.methods.withdrawEth(accounts[2], etherAmount).send({
            from: accounts[0],
            gas: '1000000'
        });

        const balanceOfBeneficiary = await web3.eth.getBalance(accounts[2]);
        assert.equal(balanceOfBeneficiary, web3.utils.toWei('105', 'ether'));
    });

    it('2.2.0 Buy tokens from contract', async () => {
        const addressContract = esb.options.address;
        await web3.eth.sendTransaction({
            from: accounts[5],
            to: addressContract,
            value: web3.utils.toWei('5', 'ether')
        });

        const price = await esb.methods.price().call();
        const numberTokensExpected = web3.utils.toWei((5 * price / 100).toString(),'ether');
        const numberTokensReceived = await esb.methods.balanceOf(accounts[5]).call();
        assert.equal(numberTokensExpected, numberTokensReceived);

        const balanceOfBeneficiary = await web3.eth.getBalance(accounts[5]);
        assert(balanceOfBeneficiary > web3.utils.toWei('94', 'ether'));

    });

    it('2.2.1 Refund tokens back to contract', async () => {
        const addressContract = esb.options.address;

        await web3.eth.sendTransaction({
            from: accounts[5],
            to: addressContract,
            value: web3.utils.toWei('5', 'ether')
        });

        const price = await esb.methods.price().call();

        const numberTokensToRefund = web3.utils.toWei((5 * price / 100).toString(),'ether');
        await esb.methods.transfer(addressContract, numberTokensToRefund).send({
            from: accounts[5],
            gas: '1000000'
        });

        const balanceOfRefunder = await web3.eth.getBalance(accounts[5]);
        assert(balanceOfRefunder > web3.utils.toWei('94', 'ether'));
    });

    it('2.2.2 Right balance', async () => {
        const addressContract = esb.options.address;

        await web3.eth.sendTransaction({
            from: accounts[5],
            to: addressContract,
            value: web3.utils.toWei('5', 'ether')
        });

        const price = await esb.methods.price().call();

        const numberTokensReceived = await esb.methods.balanceOf(accounts[5]).call();
        const numberTokensExpected = web3.utils.toWei((5 * price / 100).toString(),'ether');
        assert.equal(numberTokensReceived, numberTokensExpected);

        const numberTokensInContract = await esb.methods.balanceOf(addressContract).call();
        const totalSupplyTokens = await esb.methods.totalSupply().call();
        const remainingTokensInSupply = totalSupplyTokens - numberTokensReceived;
        assert.equal(remainingTokensInSupply, numberTokensInContract);
    });

    it('2.2.3 Wei raised', async () => {
        const addressContract = esb.options.address;

        await web3.eth.sendTransaction({
            from: accounts[5],
            to: addressContract,
            value: web3.utils.toWei('5', 'ether')
        });

        const weiRaised = await esb.methods.weiRaised().call();
        const weiRaisedExpected = web3.utils.toWei('5', 'ether');
        assert.equal(weiRaised, weiRaisedExpected);

    });

});